const SECRETO_JWT = 'secreto'
const CONFIG_JWT = {
    expiresIn :'60s'
}

export {
    SECRETO_JWT,
    CONFIG_JWT
}
