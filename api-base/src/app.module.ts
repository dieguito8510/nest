import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AutenticacionModule } from './autenticacion/autenticacion.module';
import { UsuarioModule } from './usuario/usuario.module';


@Module({
  imports: [UsuarioModule,AutenticacionModule,],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
