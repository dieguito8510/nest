import { Controller, Post, Body, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {AutenticacionService} from "./autenticacion.service";

@Controller('autenticacion')
export class AutenticacionController {

    constructor(private readonly autenticacionService: AutenticacionService) {
    }

    @UseGuards(AuthGuard('local'))
    @Post('/login')
    login(@Body() credenciales: CredencialesInterface){
        const jwt = this.autenticacionService.loginConCredecianles(credenciales)
        return {
            access_token: jwt
        }
    }
}

interface CredencialesInterface {
    usuario:string;
    password: string;
}
