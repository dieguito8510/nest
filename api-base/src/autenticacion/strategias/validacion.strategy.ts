import {PassportStrategy} from "@nestjs/passport";
import { Strategy } from "passport-local";
import {Injectable, UnauthorizedException} from "@nestjs/common";
import { UsuarioService } from "src/usuario/usuario.service";

@Injectable()
export class ValidacionStrategy extends PassportStrategy(Strategy) {
    constructor(
        private readonly userService: UsuarioService
    ) {
        super({
            usernameField: 'name',
            passwordField: 'password'
        });
    }

    async validate(usuario, password) {
        const listaUsuarios = this.userService.listarUsuarios();
        const usuarioEncontrado = listaUsuarios.filter(
            (user: any) =>
                user.name === usuario && user.password === password)
            console.log('usuario encontrado: ', usuarioEncontrado)
        if(usuarioEncontrado.length > 0) {
            return usuarioEncontrado[0]
        }else {
            throw new UnauthorizedException({
                mensaje: 'Usuario no encontrado en el sistema'
            })
        }
    }
}