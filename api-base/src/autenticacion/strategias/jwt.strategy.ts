import {ExtractJwt, Strategy} from "passport-jwt";
import {PassportStrategy} from "@nestjs/passport"
import {SECRETO_JWT} from "../../constantes/configuracion";
export class JwtStrategy extends PassportStrategy(Strategy){

    constructor() {
        super({
            jwtFromRequest : ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey: SECRETO_JWT
        });
    }

    async validate(payload){
        return {nombreUsuario: payload.nombreUsuario, password: payload.password}
    }

}
