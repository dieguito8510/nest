import { Module } from '@nestjs/common';
import { AutenticacionService } from './autenticacion.service';
import { AutenticacionController } from './autenticacion.controller';
import {JwtModule} from "@nestjs/jwt";
import {CONFIG_JWT, SECRETO_JWT} from "../constantes/configuracion";
import { JwtStrategy } from './strategias/jwt.strategy';
import {ValidacionStrategy} from "./strategias/validacion.strategy";
import { UsuarioModule } from 'src/usuario/usuario.module';


@Module({
  imports: [
    JwtModule.register({
      secret: SECRETO_JWT,
      signOptions: CONFIG_JWT
    }),
    UsuarioModule
  ],
  providers: [AutenticacionService,JwtStrategy, ValidacionStrategy],
  controllers: [AutenticacionController]
})
export class AutenticacionModule {}
