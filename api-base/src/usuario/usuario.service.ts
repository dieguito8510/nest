import { Injectable } from '@nestjs/common';
import {USUARIOS} from "../constantes/usuarios";

@Injectable()
export class UsuarioService {
    usuarios = USUARIOS;

    constructor() {
        // console.log('usuarios', this.usuarios)
    }

    listarUsuarios(){
        return this.usuarios
    }

    buscarPorId(id: string){
        const usuarioEncontrado = this.usuarios
            .filter((usuario)=> usuario._id === id );
        // me va a entregar el usuario encontrado o [] si no encontro
        return usuarioEncontrado;
    }

    eliminarPorId(id: string){
        const usuarioEncontrado = this.usuarios
            .filter((usuario)=> usuario._id === id );
        if(usuarioEncontrado.length > 0){
            const indice = this.usuarios.indexOf(usuarioEncontrado[0])
            const usuarioEliminado = this.usuarios.splice(indice,1)
            console.log('usuario eliminado')
            return true
        }else {
            return false
        }


    }

    crearUsuario(datosUsuario){
        this.usuarios.push(datosUsuario);
        return this.usuarios
    }

    actualizar(id, datosUsuario){
        const usuarioEncontrado = this.usuarios
            .filter((usuario)=> usuario._id === id );
        if(usuarioEncontrado.length > 0) {
            const indice = this.usuarios.indexOf(usuarioEncontrado[0])
            this.usuarios[indice] = datosUsuario
            return this.usuarios[indice]
        }else {
            return false
        }
    }
}