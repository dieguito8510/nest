import {Controller, Get, Delete, Post, Put, Param, Body, NotFoundException, BadRequestException,UseGuards} from '@nestjs/common';
import {UsuarioService} from "./usuario.service";
import {JwtAuthGuard} from "../autenticacion/guards/jwt-auth.guard";

@Controller('usuario')
export class UsuarioController {

    constructor(private readonly usuarioService: UsuarioService) {
    }

    @UseGuards(JwtAuthGuard)
    @Get('/lista-usuarios')
    getUsuarios(){
        const usuarios = this.usuarioService.listarUsuarios()
        return {
            mensaje: "lista de usuarios",
            datos: usuarios
        }
    }


    @Delete('/eliminar/:id')
    eliminarUsuario(@Param('id') idUsuario){
        console.log('id en eliminar', idUsuario)
        const usuarioEliminado = this.usuarioService.eliminarPorId(idUsuario)
       if(usuarioEliminado) {
           return {
               mensaje: 'usuario eliminado',
               usuario : usuarioEliminado

           }
       }else {
           throw new BadRequestException()
       }

    }

    
    @Get(':id')
    getUsuarioPorId(@Param('id') idUsuario){
        const usuarioEncontrado = this.usuarioService.buscarPorId(idUsuario)
        if(usuarioEncontrado.length > 0){
            return {
                mensaje: "Busqueda por id",
                usuario: usuarioEncontrado
            }
        }else {
            throw new NotFoundException();
        }
    }


    @Post('crear')
    crearUsuario(@Body() datosUsuario){
        console.log('datos post', datosUsuario)
        const usuarioCreado = this.usuarioService.crearUsuario(datosUsuario)
        return {
            mensaje: 'usuario creado',
            datos: usuarioCreado
        }
    }


    @Put('actualizar/:id')
    actualizarUsuario(@Body() datosUsuario,
                      @Param('id') id){
        const usuarioActualizado = this.usuarioService.actualizar(id, datosUsuario)
        if(usuarioActualizado){
            return {
                mensaje: 'usuario actualizado',
                datos: datosUsuario,
                idUsuario: id
            }
        }else {
            throw new BadRequestException()
        }
    }   
}