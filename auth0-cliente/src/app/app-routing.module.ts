import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RutaErrorComponent } from './ruta-error/ruta-error.component';
import { RutaInicioComponent } from './ruta-inicio/ruta-inicio.component';
import { RutaUsuarioComponent } from './ruta-usuario/ruta-usuario.component';
import {AuthGuard} from "@auth0/auth0-angular";

const routes: Routes = [
  {
    path: 'inicio',
    component: RutaInicioComponent
  },
  {
    path:'usuarios',
    component: RutaUsuarioComponent,
    canActivate: [AuthGuard]
  },
  {
    path:'',
    redirectTo: 'inicio',
    pathMatch: 'full'
  },
  {
    path: '**',
    component: RutaErrorComponent,
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

