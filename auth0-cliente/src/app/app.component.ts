import {Component, OnInit} from '@angular/core';
import {AuthService} from "@auth0/auth0-angular";
import {environment} from "../environments/environment";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'auth0-cliente';

  estaAutenticado = false;
  usuario: any;
  constructor(private readonly auth0Service: AuthService) {
  }

  ngOnInit(): void {
    // se ejecuta siempre que se inicia el componente
    this.verifarUsuarioAutenticado();
    this.obtenerUsuario();
  }

  verifarUsuarioAutenticado(){
    this.auth0Service.isAuthenticated$
      .subscribe(
        respuesta => {
          this.estaAutenticado = respuesta;
        }
      )
  }

  obtenerUsuario(){
    this.auth0Service.user$
      .subscribe(
        respuesta => {
          console.log('usuario respuesta', respuesta)
          this.usuario = respuesta;
        }
      )
  }

  login(){
    this.auth0Service.loginWithRedirect();
  }

  logout(){
    this.auth0Service.logout({returnTo:environment.auth0.redirectUri});
  }


}
